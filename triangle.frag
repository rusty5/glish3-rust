#version 330 core

out vec4 Color;
uniform sampler2D test;
in vec2 uv;

void main()
{
    Color = texture(test, uv);
}