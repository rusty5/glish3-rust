use std::os::raw::{c_uint,c_float};
use libc::size_t;
use gl::types::GLenum;
#[repr(C)]
pub struct VboMoc{
	_private: [u8;0],
}

#[repr(C)]
pub struct Settings{
	pub size: c_uint,
	pub index :c_uint,
	pub stride:c_uint,
	pub begin:c_uint,
}

impl Default for Settings{
	fn default() -> Self{
		Settings{size:0, index:0, stride:0, begin:0}
	}
}
pub struct Vbo{
	pub vbo_moc: *mut VboMoc,
}
impl Vbo{
	pub fn new(target : GLenum, data: &mut [f32], settings:&[Settings])-> Self{
		Vbo{vbo_moc: unsafe{create_vbo(target, data.as_mut_ptr(), data.len() as size_t, settings.as_ptr(), settings.len() as size_t)}}
	}
}
impl Drop for Vbo{
	fn drop(&mut self){
		unsafe{destroy_vbo(self.vbo_moc)};
	}
}

extern "C"{
	fn create_vbo(target :GLenum , data: *mut c_float, size: size_t, sets:* const Settings, set_counts : size_t)-> *mut VboMoc;

	fn destroy_vbo(vbo:*mut VboMoc);
}