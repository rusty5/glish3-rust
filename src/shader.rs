extern crate libc;
use gl::types::GLenum;
use std::os::raw::c_char;
use crate::utils::to_cstr;
#[repr(C)]
pub struct ShaderMoc{
    _private:[u8;0],
}
extern "C"{
    fn create_shader() -> *mut ShaderMoc;
    fn create_shader_from_file(shader: *mut ShaderMoc, shader_type :GLenum, file: *const c_char);
    fn create_shader_from_data(shader: *mut ShaderMoc, shader_type :GLenum, file: *const c_char);
    fn destroy_shader(shader:*mut ShaderMoc);
}
pub struct Shader{
    pub _shader: *mut ShaderMoc,
}


impl Shader{
  
    pub fn create_from_file(shader_type: GLenum, file_path: &str)->Self{
		let shader = Shader::default();
		unsafe{
			create_shader_from_file(shader._shader, shader_type, to_cstr(&file_path).as_ptr());
		}
		return shader;
    }
    pub fn create_from_data(shader_type: GLenum, file_path: &str)->Self{
        let shader = Shader::default();
		unsafe{
			create_shader_from_data(shader._shader, shader_type, to_cstr(&file_path).as_ptr());
		}
		return shader;
    }
}
impl Default for Shader{
    fn default()->Self{
        Shader{_shader:unsafe{create_shader()}}
    }
}
impl Drop for Shader{
    fn drop(&mut self){
        unsafe{
            destroy_shader(self._shader);
        }
    }
}
