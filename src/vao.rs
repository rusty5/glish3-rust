use crate::vbo::*;
#[repr(C)]
struct VaoMoc {
    _private: [u8; 0],
}
pub struct Vao {
    vao: *mut VaoMoc,
}
impl Vao {
   
    pub fn bind(&mut self) {
        unsafe {
            bind_vao(self.vao);
        }
    }
    pub fn add_vbo(&mut self, vbo: Vbo){
        unsafe{
            add_vbo(self.vao, vbo.vbo_moc);
        }
    }
}
impl Default for Vao{
    fn default() -> Self {
        unsafe {
            return Vao { vao: create_vao() };
        }
    }
}
impl Drop for Vao {
    fn drop(&mut self) {
        unsafe {
            delete_vao(self.vao);
        }
    }
}
extern "C" {
    fn create_vao() -> *mut VaoMoc;
    fn bind_vao(vao: *mut VaoMoc);
    fn delete_vao(vao: *mut VaoMoc);
    fn add_vbo(vao: *mut VaoMoc, vbo: *mut VboMoc);
}
