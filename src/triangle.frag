#version 330 core

out vec4 Color;
uniform float test;
void main()
{
    Color = vec4(test, 0.5f, 0.2f, 1.0f);
}