use crate::utils;
use std::os::raw::{c_char, c_int};
extern "C"{
	fn create_texture2d(path_image:*const c_char)->*mut Texture2dMoc;
	fn destroy_texture2d(texture:*mut Texture2dMoc);
	fn activate_texture2d(texture: *mut Texture2dMoc, id: c_int);
}

#[repr(C)]
struct Texture2dMoc{
	_private: [u8;0],
}

pub struct Texture2d{
	texturemoc: *mut Texture2dMoc,
}

impl Texture2d{
	pub fn new(path_image: &str)-> Self{
		return Texture2d{texturemoc:
			unsafe{create_texture2d(utils::to_cstr(path_image).as_ptr())}
		};
	}
	pub fn activate(&mut self, id: i32){
		unsafe{
			activate_texture2d(self.texturemoc, id as c_int);
		}
	}
}
impl Drop for Texture2d{	
	fn drop(&mut self){
		unsafe {destroy_texture2d(self.texturemoc)};
	}

}
pub struct TextureView{
	texture: *mut Texture2dMoc
}
impl TextureView{
	pub fn new(texture2d: &Texture2d)->Self{
		TextureView{texture:texture2d.texturemoc}
	}
	pub fn activate(&mut self, id :i32){
		unsafe{
			activate_texture2d(self.texture, id as c_int);
		}
	}
}