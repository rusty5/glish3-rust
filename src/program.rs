use crate::shader::{Shader, ShaderMoc};
use std::os::raw::{c_int, c_void, c_char};
use crate::utils::to_cstr;
extern "C"{
	fn create_program_default() -> *mut ProgramMoc;
	fn create_program(prog: *mut ProgramMoc, count: c_int, shaders: *const*mut ShaderMoc);
	fn delete_program(prog:*mut ProgramMoc);
	fn use_program(prog:*mut ProgramMoc);
	fn update_uniform(prog: *mut ProgramMoc, uniform_name:*const c_char, data:*const c_void);
}

#[repr(C)]
struct ProgramMoc{
	_private: [u8;0],
}

pub struct Program{
	_program: *mut ProgramMoc,
}
impl Program{
	pub fn new(shaders : &[Shader])-> Self{
		let program = Program::default();
		let mut shaders_moc: Vec<*mut ShaderMoc> = Vec::with_capacity(shaders.len());
		for shader in shaders.iter() {
			shaders_moc.push(shader._shader);
		}
		unsafe {
			create_program(program._program, shaders.len() as c_int,shaders_moc.as_ptr());
		}
		return program;
	}

	pub fn using(&self){
		unsafe{use_program(self._program)};
	}
	pub fn update<T>(&mut self, uniform_name: &str, data: &[T]){
		unsafe{
			update_uniform(self._program, to_cstr(&uniform_name).as_ptr(), data.as_ptr() as *const c_void);
		}
	}
}
impl Default for Program{
	fn default() -> Self{
		Program{_program:unsafe{create_program_default()}}
	}
}
impl Drop for Program{
	fn drop(&mut self){
		unsafe{
			delete_program(self._program);
		}
	}
}