extern crate sdl2;
use glish3::window;
use glish3::shader;
use gl;
use glish3::*;
use texture2d::*;
fn main() {
    let sdl = sdl2::init().unwrap();
    
    let window_sdl2 = window::Window::default();
	let mut event_pump = sdl.event_pump().unwrap();
	let vertex_shader = shader::Shader::create_from_file(gl::VERTEX_SHADER, "triangle.vert"); 
	let frag_shader = shader::Shader::create_from_file(gl::FRAGMENT_SHADER, "triangle.frag"); 

	let mut prog = program::Program::new(&[vertex_shader, frag_shader]); 
	let mut vao = vao::Vao::default();
    let mut data:[f32;8] = [0., 0., 
                        1., 0.,
						0., -1.,
						1., -1.];
    let vbo_settings = vbo::Settings{
        size:2,
        ..
        vbo::Settings::default()
	};
	
	let mut texture = Texture2d::new(&"de.png");
	texture.activate(0);
    vao.add_vbo(vbo::Vbo::new(gl::ARRAY_BUFFER, &mut data, &[vbo_settings]));
	prog.using();
	let test:[i32;1] = [0];
	prog.update("test", &test);
    'main: loop {
        for event in event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => break 'main,
                _ => {}
            }
        }
		// render window contents here
        window_sdl2.clean();
        unsafe{
            utils::drawArrays(
                gl::TRIANGLE_STRIP, // mode
                0, // starting index in the enabled arrays
                4 // number of indices to be rendered
            );
        }
        window_sdl2.refresh();
    }
}
