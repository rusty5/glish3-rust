extern "C" {
    fn create_window() -> *mut WindowMoc;
    fn refresh(win: *mut WindowMoc);
	fn destroy(win: *mut WindowMoc);   
	fn clean(win: *mut WindowMoc);
}

#[repr(C)]
struct WindowMoc{
    _private:[u8;0],
}

pub struct Window{
    window: *mut WindowMoc,
}

impl Default for Window{
    fn default() -> Self{
        Window{window : unsafe{create_window()}}
    }

}
impl Window{
    pub fn refresh(&self){
        unsafe{
            refresh(self.window);
        }
	}
	pub fn clean(&self){
		unsafe{
			clean(self.window);
		}
	}
}
impl Drop for Window{
    fn drop(&mut self){
        unsafe{
            destroy(self.window);
        }
    }
}

