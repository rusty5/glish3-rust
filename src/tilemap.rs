use crate::texture2d::*;
extern crate serde_json;
use std::fs::File;
use std::io::Read;
use std::path::Path;
pub struct Transform{
    pub x:i32,
    pub y:i32,
    pub width:i32,
    pub heigth:i32
}
pub struct Texture<'a>{
    pub transform: Transform,
    pub texture2d: &'a mut Texture2d,
}
pub struct SpriteData{
    pub u: f32,
    pub v: f32,
    pub width: f32,
    pub heigth: f32,
}
pub struct SpriteSheetData{
    pub sprites_data: Vec::<SpriteData>,
}
pub struct Sprite<'a>{
    pub data_texture: Texture<'a>,
    pub sprite_number :i32,
}
pub struct TileMap<'a>{
    texture : Texture2d,
    sprites : Vec::<Sprite<'a>>,
    sprite_sheet : SpriteSheetData
}

fn read_json(path_to_map: &str)->serde_json::Value {

    let mut file = File::open(path_to_map).unwrap();
    let mut content  = String::new();
    file.read_to_string(&mut content).unwrap();
    // Read the JSON contents of the file as an instance of `User`.
    return serde_json::from_str(&content).unwrap();
}

// impl<'a> TileMap<'a>{
//     fn new(path_to_map: &str)-> Self{
//         let value = read_json(&path_to_map);
//         struct TilemapParam{
//             nb_tile_height: i64,
//             nb_tile_width: i64,
//             tile_height: i64,
//             tile_width: i64,
//         };
//         let tile_param = TilemapParam{
//             nb_tile_height: value["height"].as_i64().unwrap(),
//             nb_tile_width :value["width"].as_i64().unwrap(),
//             tile_height :value["tileheight"].as_i64().unwrap(),
//             tile_width :value["tilewidth"].as_i64().unwrap(),
//         };
//         let mut path_image = Path::new(&path_to_map).parent().unwrap();
//         path_image.join(Path::new(&value["tilesets"][0]["image"].to_string()));
//         let texture2d = Texture2d::new(&path_image.to_str().unwrap());


        
//     }
// }

