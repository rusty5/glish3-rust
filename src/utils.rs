use std::ffi::{CString};
use gl::types::*;
pub fn to_cstr(rust_string: &str) -> CString{
	// allocate buffer of correct size
	let buffer: Vec<u8> = Vec::from(rust_string);
	// convert buffer to CString
	return unsafe { CString::from_vec_unchecked(buffer) };
}
extern "C"{
    pub fn drawArrays(mode :GLenum,first: GLint, count :GLsizei );
}