use cmake;

fn build_and_link(lib: String) {
    let dst = cmake::build(&lib);
    println!("cargo:rustc-link-search=native={}", dst.display());
    println!("cargo:rustc-link-lib=static={}", lib);
}
fn main() {
    build_and_link("render-library".to_string());
	println!("cargo:rustc-flags=-l dylib=glish3");
    println!("cargo:rustc-flags=-l dylib=SDL2");
	println!("cargo:rustc-flags=-l dylib=utils");
    println!("cargo:rustc-flags=-l dylib=GL");
    println!("cargo:rustc-flags=-l dylib=GLU");
	println!("cargo:rustc-flags=-l dylib=GLEW");
    println!("cargo:rustc-flags=-l dylib=stdc++");
}
